<?php
function _uniord($c){
    if(ord($c{0}) >=0 && ord($c{0}) <= 127)
        return ord($c{0});
    if(ord($c{0}) >= 192 && ord($c{0}) <= 223)
        return (ord($c{0})-192)*64 + (ord($c{1})-128);
    if(ord($c{0}) >= 224 && ord($c{0}) <= 239)
        return (ord($c{0})-224)*4096 + (ord($c{1})-128)*64 + (ord($c{2})-128);
    if(ord($c{0}) >= 240 && ord($c{0}) <= 247)
        return (ord($c{0})-240)*262144 + (ord($c{1})-128)*4096 + (ord($c{2})-128)*64 + (ord($c{3})-128);
    if(ord($c{0}) >= 248 && ord($c{0}) <= 251)
        return (ord($c{0})-248)*16777216 + (ord($c{1})-128)*262144 + (ord($c{2})-128)*4096 + (ord($c{3})-128)*64 + (ord($c{4})-128);
    if(ord($c{0}) >= 252 && ord($c{0}) <= 253)
        return (ord($c{0})-252)*1073741824 + (ord($c{1})-128)*16777216 + (ord($c{2})-128)*262144 + (ord($c{3})-128)*4096 + (ord($c{4})-128)*64 + (ord($c{5})-128);
    if(ord($c{0}) >= 254 && ord($c{0}) <= 255)    //  error
        return FALSE;
    return 0;
}

function arr2b_dechex($n){
    return str_pad(dechex($n*2), 2, '0', STR_PAD_LEFT);
}

if(($handle = fopen(__DIR__ . "/../WEToolsV3.source.js", "r")) === false){
    echo "Failed to open source file.\n";
    exit(1);
} else {
    echo "Source code loaded successfully\n";
}

$code = [];
while(($line = fgets($handle)) !== false)
    $code[] = $line;

fclose($handle);

if(($handle = fopen(__DIR__ . "/user.list", "r")) === false){
    echo "Failed to open user list.\n";
    exit(2);
} else {
    echo "Users list loaded successfully\n";
}

$users = [];
while(($line = fgets($handle)) !== false) {
    $line = trim($line);
    if(strlen($line) < 1){
        continue;
    }
    $users[] = explode(';;', $line);
}

fclose($handle);
// var_dump($users, $code); exit;
for($x=0, $ucount=count($users); $x<$ucount; $x++){
    printf("Processing user #%d - %s\nGenerating secrets\n", $users[$x][1], $users[$x][0]);
    $factor = $users[$x][1]%128;
    $pass   = $users[$x][0]."_".(($factor << 4)+$factor)."_".$users[$x][1];
    $salt   = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx     = '';
    while(strlen($salted) < 48){
        $dx = md5($dx.$pass.$salt, true);
        $salted .= $dx;
    }

    $indexes = [0, 32, 32, 16, strlen($salted), strlen($salt)];
    $source .= implode(chr(6), array_map('arr2b_dechex', $indexes)).chr(3);
    $key = substr($salted, $indexes[0], $indexes[1]);
    $iv  = substr($salted, $indexes[2], $indexes[3]);

    $source .= bin2hex($salted).bin2hex($salt);
    for($i=0, $c=strlen($source); $i<$c; $i++)
        $source[$i] = rand(0,100)>50 && preg_match("/^[a-z]$/", $source[$i]) ? strtoupper($source[$i]) : $source[$i];

    echo "Preparing source\n";
    $source = 'if(ww[ww[358155..toString(32)]("R2FtZQ==")][ww[358155..toString(32)]("cGxheWVyX2lk")]==0x'.dechex($users[$x][1]).'){eval("'.chr(2).$source.'\n'.chr(3);
    for($y=0, $ccount=count($code); $y<$ccount; $y++)
        $source .= base64_encode(openssl_encrypt($code[$y], 'aes-256-cbc', $key, true, $iv)).'\n';

    echo "Calculating image resolution\n";
    $source = substr($source, 0, -2).'");}';
    $source = preg_split('//u', $source, -1, PREG_SPLIT_NO_EMPTY);
    $count  = count($source) + 3* 1;
    while($count%3){
        $source[] = 0;
        $count++;
    }

    $length[1] = $length[0] = ceil(sqrt($count/3));
    if($length[0]*($length[0]-1) > $count/3)
        $length[1]--;

    echo "Initializing image\n";
    $image = imagecreatetruecolor($length[0], $length[1]);
    $rgb   = [0,0,0];
    $pixel = 0;
    imagesavealpha($image, true);
    imagealphablending($image, false);

    // fill transparent - default init
    $color  = imagecolorallocatealpha($image,0,0,0,127);
    imagefilledrectangle($image, 0, 0, $length[0], $length[1], $color);
    imagealphablending($image, true);

    echo "Setting data pixel\n";
    // first pixel - data pixel - pixel count
    $color = imagecolorallocate($image, ($count/3)%255, ($count/3-($count/3)%255)/255, 0);
    imagesetpixel($image, $pixel++, 0, $color);
    imagealphablending($image, true);

    echo "Generating image\n";
    $count -= 3* 1;
    for($y=0; $y<$count; $y++){
        $rgb[$y%3] = (_uniord($source[$y])+$factor)%255;

        if($y%3 == 2){
            $color = imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]);
            imagesetpixel($image, $pixel%$length[0], ($pixel-$pixel%$length[0])/$length[1], $color);
            imagealphablending($image, true);
            $rgb = [0,0,0];
            $pixel++;
        }
    }

    $imgPath = __DIR__ . "/imgout/{$users[$x][1]}.png";
    printf("Saving image in %s\n", $imgPath);
    imagepng($image, $imgPath);
    imagedestroy($image);
    echo "Image saved successfully\n\n";
}
