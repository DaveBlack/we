##### PERFORMANCE
- [ ] automate build - move to CI
- [ ] garbage collection
- [ ] code optimization (decrease RAM usage)

##### SECURITY
- [x] compile encrypted code to images
    - [ ] public & private key?
- [ ] per-user CSS (as it can not be encrypted)
    - [ ] placeholders in code & CSS to modify element IDs etc

##### Forum
- [ ] copy/move single topic directly from detail
- [ ] copy more post than only the 1st
    - [ ] checkboxes on the bottom left
        - [ ] checkbox - quote original author
- [ ] after copying set up sync link between original and copy
- [ ] after copying jump to destination forum
    - [ ] probably this should have a checkbox in settings
- [ ] sync requests refactoring
- [ ] auto post to SOS forum when a revolt has started
    - [ ] or by a button??
    - [ ] include GRCR report

##### Map
- [ ] show icons over farm towns showing trading resources
    - [ ] show available colonization spots on strategic map
        - [ ] political map (smaller strategic map with approx. SVG areas colored)
    - [ ] player profile link in map (town context menu)

##### Units overview
- [ ] land units strength (town overview & barracks) [[1]][1]
- [ ] naval units strength (town overview & barracks) [[1]][1]
- [ ] transport capacity [[1]][1]
    - [ ] unit strength overview window [[1]][1]
    - [ ] switch buttons [[2]][2] (town/outside etc.)

##### Farm towns (auto)
- [ ] settings - select first all (towns)
- [ ] nocaptain autofarm
    - [ ] unit farming
        - [ ] farming conditions (if storage full then farm units)

##### Sentinels
- [ ] add reload icon
- [ ] add send all missing for islands
- [ ] after clicking the send icon hide the previous support window
- [ ] settings
    - [ ] add multiselect? for additional alliances to be included
- [ ] context menu
    - [ ] add a smaller icon over the support icon for sending sentinel (autofill)

##### Attack
- [ ] psychical terror AKA 'annoying module' - send a minimal attack then cancel it after X seconds (settings)
- [ ] colony ship identifier - if there is a new attack incoming and it is possibly containing a colony ship then mark it some way
    - [ ] persistence

##### World wonders
- [ ] auto resend
    - [ ] ability to stop
- [ ] make it independent [[1]][1]

##### Further automation (auto)
- [ ] auto culture module
- [ ] auto building [[3]][3]
- [ ] auto attack [[3]][3]
- [ ] auto defense [[3]][3]
    - [ ] save attack units (by sending out or attacking the attacker)

##### Browser notifications
- [ ] do not show after town change
- [ ] attack - counts by town
    - [ ] support type support

##### In-game notifications
- [ ] update notifications

##### Public
- [ ] rename full version to 'we-superhero'
    - [ ] port allowed features to separate module as 'we-tools'

##### Miscellaneous
- [ ] trading overview
    - [ ] auto build farms
- [ ] WYSIWYG editor - forum/messages/notes
- [ ] town list groups reorder & resize jQUery UI
- [ ] messages quotation
- [ ] BUG town list population percentage [[1]][1]
- [ ] color fill alliance flags [[2]][2] (forum, messages, notes)
    - [ ] improved troop speed
    - [ ] simulator [[1]][1] (layout, unit strength etc.)
- [ ] port features from [DIO][1] & [Mole Hole][2] & [MeatScript][3]
- [ ] move localization strings into ISO 3166-1 alpha-2 indexed object
    - [ ] ln10 defaults as 'en'

[1]: https://web.archive.org/web/20181228154926/http://diotools.de/
[2]: https://grmh.pl/
[3]: https://autofarmbot.meatscripts.com/
